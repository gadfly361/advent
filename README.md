# advent

This repository is in response to a technical assessment and answers days 1, 2, and 4 of [http://adventofcode.com](http://adventofcode.com).

To run the tests:

```
lein test
```

## [Advent of code Day 1](http://adventofcode.com/2017/day/1)

### Part 1

To recap the problem:

> The captcha requires you to review a sequence of digits (your puzzle
> input) and find the sum of all digits that match the next digit in the
> list. The list is circular, so the digit after the last digit is the
> first digit in the list.

> For example:
>
> 1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2) matches the fourth digit.
> 1111 produces 4 because each digit (all 1) matches the next.
> 1234 produces 0 because no digit matches the next.
> 91212129 produces 9 because the only digit that matches the next one is the last digit, 9.

My solution can be found [here](./src/advent/day_01.clj).

###  Part 2

> Now, instead of considering the next digit, it wants you to consider the digit halfway around the circular list. That is, if your list contains 10 items, only include a digit in your sum if the digit 10/2 = 5 steps forward matches it. Fortunately, your list has an even number of elements.

> For example:

> 1212 produces 6: the list contains 4 items, and all four digits match the digit 2 items ahead.
> 1221 produces 0, because every comparison is between a 1 and a 2.
> 123425 produces 4, because both 2s match each other, but no other digit has a match.
> 123123 produces 12.
> 12131415 produces 4.

My solution can be found [here](./src/advent/day_01.clj).


## [Advent of code Day 2](http://adventofcode.com/2017/day/2)

### Part 1

To recap the problem:

> The spreadsheet consists of rows of apparently-random numbers. To make sure the recovery process is on the right track, they need you to calculate the spreadsheet's checksum. For each row, determine the difference between the largest value and the smallest value; the checksum is the sum of all of these differences.

> For example, given the following spreadsheet:

> 5 1 9 5

> 7 5 3

> 2 4 6 8

> The first row's largest and smallest values are 9 and 1, and their difference is 8.
> The second row's largest and smallest values are 7 and 3, and their difference is 4.
> The third row's difference is 6.
> In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

My solution can be found [here](./src/advent/day_02.clj).

### Part 2

To recap the problem:

> It sounds like the goal is to find the only two numbers in each row where one evenly divides the other - that is, where the result of the division operation is a whole  number. They would like you to find those numbers on each line, divide them, and add up each line's result.

> For example, given the following spreadsheet:

> 5 9 2 8

> 9 4 7 3

> 3 8 6 5

> In the first row, the only two numbers that evenly divide are 8 and 2; the result of this division is 4.
> In the second row, the two numbers are 9 and 3; the result is 3.
> In the third row, the result is 2.
> In this example, the sum of the results would be 4 + 3 + 2 = 9.

My solution can be found [here](./src/advent/day_02.clj).


## [Advent of code Day 4](http://adventofcode.com/2017/day/4)

### Part 1

To recap the problem:

> A new system policy has been put in place that requires all
> accounts to use a passphrase instead of simply a password. A
> passphrase consists of a series of words (lowercase letters)
> separated by spaces.

> To ensure security, a valid passphrase must contain no duplicate words.

> For example:

> aa bb cc dd ee is valid.
> aa bb cc dd aa is not valid - the word aa appears more than once.
> aa bb cc dd aaa is valid - aa and aaa count as different words.

My solution can be found [here](./src/advent/day_04.clj).

### Part 2

> For added security, yet another system policy has been put in place. Now, a valid passphrase must contain no two words that are anagrams of each other - that is, a passphrase is invalid if any word's letters can be rearranged to form any other word in the passphrase.

> For example:

> abcde fghij is a valid passphrase.
> abcde xyz ecdab is not valid - the letters from the third word can be rearranged to form the first word.
> a ab abc abd abf abj is a valid passphrase, because all letters need to be used when forming another word.
> iiii oiii ooii oooi oooo is valid.
> oiii ioii iioi iiio is not valid - any of these words can be rearranged to form any other word.
> Under this new system policy, how many passphrases are valid?

My solution can be found [here](./src/advent/day_04.clj).


# LICENSE

The MIT License (MIT)

Copyright © 2018 Matthew Jaoudi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
