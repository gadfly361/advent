(defproject advent "0.1.0-SNAPSHOT"
  :license {:name "MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.csv "0.1.2"]]

  :profiles
  {:dev
   {:plugins [[com.jakemccrary/lein-test-refresh "0.22.0"]]}})
