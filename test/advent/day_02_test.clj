(ns advent.day-02-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [advent.day-02 :as target]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest row->checksum
  (testing "When row is empty"
    (is (= 0
           (target/row->checksum nil))
        "Should be zero if row is nil")

    (is (= 0
           (target/row->checksum []))
        "Should be zero if row is empty"))


  (testing "When row has a single value"
    (testing "Should be zero"
      (is (= 0
             (target/row->checksum [0])))

      (is (= 0
             (target/row->checksum [10])))))


  (testing "When row has values that are string versions of ints"
    (testing "Should parse strings and read as ints"
      (is (= 0
             (target/row->checksum ["0"])))

      (is (= 0
             (target/row->checksum ["10"])))

      (is (= 10
             (target/row->checksum ["0" "10"])))

      (is (= 10
             (target/row->checksum ["0" "10" "1" "2"])))))


  (testing "When row has two or move values"
    (testing "Should be difference between min and max value"
      (is (= 0
             (target/row->checksum [10 10])))

      (is (= 10
             (target/row->checksum [0 10])))

      (is (= 10
             (target/row->checksum [0 5 10])))

      (is (= 10
             (target/row->checksum [0 10 5])))

      (is (= 10
             (target/row->checksum [0 10 1 2 3 4 5 6 7 8 9]))))))



(deftest rows->checksum
  (testing "When rows is empty"
    (is (= 0
           (target/rows->checksum nil))
        "Should be zero if rows is nil")

    (is (= 0
           (target/rows->checksum [[]]))
        "Should be zero if rows is empty"))


  (testing "When rows has a single row with a single value"
    (is (= 0
           (target/rows->checksum [[0]])))

    (is (= 0
           (target/rows->checksum [[10]]))))


  (testing "When rows has a single row with multiple values"
    (testing "Should be difference between min and max value of the row"
      (is (= 0
             (target/rows->checksum [[10 10]])))

      (is (= 10
             (target/rows->checksum [[0 10]])))

      (is (= 10
             (target/rows->checksum [[0 5 10]])))

      (is (= 10
             (target/rows->checksum [[0 10 5]])))

      (is (= 10
             (target/rows->checksum [[0 10 1 2 3 4 5 6 7 8 9]])))))


  (testing "When rows has a multiple rows"
    (testing "Should be the sum of the difference between min and max of each row"
      (is (= 0
             (target/rows->checksum [[10 10]
                                     [10 10]
                                     ])))

      (is (= 10
             (target/rows->checksum [[0 10]
                                     [10 10]
                                     ])))

      (is (= 20
             (target/rows->checksum [[0 10]
                                     [10 0]
                                     ])))

      (is (= 20
             (target/rows->checksum [[0 5 10]
                                     [0 10]
                                     ])))

      (is (= 10
             (target/rows->checksum [[0 10 5]
                                     []
                                     [10]
                                     ])))

      (is (= 20
             (target/rows->checksum [[0 10 5]
                                     []
                                     [10]
                                     [0 10 0 0]
                                     ])))

      (is (= 30
             (target/rows->checksum [[0 10 1 2 3 4 5 6 7 8 9]
                                     [0 10 1 2 3 4 5 6 7 8 9]
                                     [0 10 1 2 3 4 5 6 7 8 9]
                                     ])))))
  )




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest even-quotient
  (testing "When xs is empty"
    (is (= 0
           (target/even-quotient 10 nil))
        "Should return 0")
    )

  (testing "When xs has values"
    (testing "Should return even-quotient"
      (is (= 0
             (target/even-quotient 10 [0])))

      (is (= 10
             (target/even-quotient 10 [1])))

      (is (= 5
             (target/even-quotient 10 [2])))

      (is (= 5
             (target/even-quotient 10 [0 2])))

      (is (= 0
             (target/even-quotient 10 [0 3])))

      (is (= 3
             (target/even-quotient 9 [0 3 4 5])))
      )))



(deftest row->even-quotient
  (testing "When row is empty"
    (is (= 0
           (target/row->even-quotient nil))
        "Should be zero if row is nil")

    (is (= 0
           (target/row->even-quotient []))
        "Should be zero if row is empty"))


  (testing "When row has a single value"
    (testing "Should be zero"
      (is (= 0
             (target/row->even-quotient [0])))

      (is (= 0
             (target/row->even-quotient [10])))))


  (testing "When row has values that are string versions of ints"
    (testing "Should parse strings and read as ints"
      (is (= 0
             (target/row->even-quotient ["0"])))

      (is (= 0
             (target/row->even-quotient ["10"])))

      (is (= 2
             (target/row->even-quotient ["5" "10"])))

      (is (= 3
             (target/row->even-quotient ["0" "9" "8" "3"])))))


  (testing "When row has two or move values"
    (testing "Should be the event quotient of the row"
      (is (= 0
             (target/row->even-quotient [0 10])))

      (is (= 10
             (target/row->even-quotient [1 10])))

      (is (= 1
             (target/row->even-quotient [10 10])))

      (is (= 2
             (target/row->even-quotient [0 5 10])))

      (is (= 3
             (target/row->even-quotient [0 3 9 10]))))))



(deftest rows->even-quotient
  (testing "When rows is empty"
    (is (= 0
           (target/rows->even-quotient nil))
        "Should be zero if rows is nil")

    (is (= 0
           (target/rows->even-quotient [[]]))
        "Should be zero if rows is empty"))


  (testing "When rows has a single row with a single value"
    (is (= 0
           (target/rows->even-quotient [[0]])))

    (is (= 0
           (target/rows->even-quotient [[10]]))))


  (testing "When rows has a single row with multiple values"
    (testing "Should be row even-quotient"
      (is (= 0
             (target/rows->even-quotient [[0 10]])))

      (is (= 10
             (target/rows->even-quotient [[1 10]])))

      (is (= 1
             (target/rows->even-quotient [[10 10]])))

      (is (= 2
             (target/rows->even-quotient [[0 5 10]])))

      (is (= 3
             (target/rows->even-quotient [[0 3 9 10]])))))


  (testing "When rows has a multiple rows"
    (testing "Should be the sum of the difference between min and max of each row"

      (is (= 1
             (target/rows->even-quotient [[0 10]
                                          [10 10]
                                          ])))

      (is (= 2
             (target/rows->even-quotient [[10 10]
                                          [10 10]
                                          ])))

      (is (= 0
             (target/rows->even-quotient [[0 10]
                                          [10 0]
                                          ])))

      (is (= 2
             (target/rows->even-quotient [[0 5 10]
                                          [0 10]
                                          ])))

      (is (= 5
             (target/rows->even-quotient [[0 10 5]
                                          []
                                          [10 9 3]
                                          ])))

      (is (= 4
             (target/rows->even-quotient [[0 10 5]
                                          []
                                          [10]
                                          [0 10 0 0]
                                          [0 8 4 0]
                                          ])))))
  )
