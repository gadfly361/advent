(ns advent.day-01-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [advent.day-01 :as target]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest number->digits
  (testing "When number is nil"
    (is (= []
           (target/number->digits nil))
        "Should be an empty vector"))

  (testing "When number is an int"
    (is (= [0]
           (target/number->digits 0))
        "Should be a vector of 0"))

  (testing "When number is an int"
    (is (= [1]
           (target/number->digits 1))
        "Should be a vector of 1"))

  (testing "When number is an int"
    (is (= [1 2]
           (target/number->digits 12))
        "Should be a vector of 1 and 2"))

  (testing "When number is an int"
    (is (= [1 2 0]
           (target/number->digits 120))
        "Should be a vector of 1, 2, and 0"))
  )



(deftest digits->digit-pairs
  (testing "When digits is nil"
    (is (= []
           (target/digits->digit-pairs nil))
        "Should be an empty vector"))


  (testing "When digits is empty vector"
    (is (= []
           (target/digits->digit-pairs []))
        "Should be an empty vector"))


  (testing "When digits is a vector of ints"
    (testing "Should be a vector of digit pairs"
      (is (= [[0 1]
              [1 0]]
             (target/digits->digit-pairs [0 1])))

      (is (= [[0 1]
              [1 2]
              [2 0]]
             (target/digits->digit-pairs [0 1 2])))

      (is (= [[0 1]
              [1 2]
              [2 0]
              [0 0]]
             (target/digits->digit-pairs [0 1 2 0])))
      )))



(deftest digit-pair-same?
  (testing "When x and y are not ints"
    (is
     (false?
      (target/digit-pair-same? nil nil))
     "When x and y are nil, should be false")

    (is
     (false?
      (target/digit-pair-same? nil 1))
     "When x is nil, should be false")

    (is
     (false?
      (target/digit-pair-same? 1 nil))
     "When y is nil, should be false"))


  (testing "When x and y are ints"
    (is
     (true?
      (target/digit-pair-same? 1 1))
     "When x and y are equal, should be true")

    (is
     (false?
      (target/digit-pair-same? 1 2))
     "When x and y are not equal, should be false"))
  )



(deftest sum-digit-pair
  (testing "When x and y are not ints"
    (is (= 0
           (target/sum-digit-pair nil nil))
        "Should be zero if x and y are nil")

    (is (= 0
           (target/sum-digit-pair nil 1))
        "Should be zero if x is nil")

    (is (= 0
           (target/sum-digit-pair 1 nil))
        "Should be zero if y is nil"))


  (testing "When x and y are ints"
    (is (= 1
           (target/sum-digit-pair 1 1))
        "Should be the value of x if x and y are equal")

    (is (= 0
           (target/sum-digit-pair nil 1))
        "Should be zero if x and y are not equal")))



(deftest sum-number
  (testing "When number is nil"
    (is (= 0
           (target/sum-number nil))
        "Should be zero if number is nil"))

  (testing "When number is an int"
    (is (= 3
           (target/sum-number 1122))
        "Should be 3 --> 1 + 2")

    (is (= 4
           (target/sum-number 1111))
        "Should be 4 --> 1 + 1 + 1 + 1")

    (is (= 0
           (target/sum-number 1234))
        "Should be 0")

    (is (= 9
           (target/sum-number 91212129))
        "Should be 9 because the last digit matches")))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest digits->digit-pairs2
  (testing "When digits is nil"
    (is (= []
           (target/digits->digit-pairs2 nil))
        "Should be an empty vector"))


  (testing "When digits is empty vector"
    (is (= []
           (target/digits->digit-pairs2 []))
        "Should be an empty vector"))


  (testing "When digits is a vector of ints"
    (testing "Should be a vector of digit pairs, where the second digit is halfway around the circular list"
      (is (= [[0 1]
              [1 0]]
             (target/digits->digit-pairs2 [0 1])))

      (is (= [[0 2]
              [1 1]
              [2 0]
              [1 1]]
             (target/digits->digit-pairs2 [0 1 2 1])))

      (is (= [[0 2]
              [1 1]
              [2 0]
              [2 0]
              [1 1]
              [0 2]]
             (target/digits->digit-pairs2 [0 1 2 2 1 0])))
      )))



(deftest sum-number2
  (testing "When number is nil"
    (is (= 0
           (target/sum-number2 nil))
        "Should be zero if number is nil"))

  (testing "When number is an int"
    (is (= 6
           (target/sum-number2 1212))
        "Should be 6 --> 1 + 2 + 1 + 2")

    (is (= 0
           (target/sum-number2 1221))
        "Should be 0")

    (is (= 12
           (target/sum-number2 123123))
        "Should be 12 --> 1 + 2 + 3 + 1 + 2 + 3")

    (is (= 4
           (target/sum-number2 12131415))
        "Should be 4 --> 1 + 0 + 1 + 0 + 1 + 0 + 1 + 0")))
