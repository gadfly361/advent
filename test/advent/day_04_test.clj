(ns advent.day-04-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [advent.day-04 :as target]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest passphrase-distinct-words?
  (testing "When passphrase is empty"
    (testing "Should be false"
      (is (false?
           (target/passphrase-distinct-words? nil)))

      (is (false?
           (target/passphrase-distinct-words? "")))))


  (testing "When passphrase has content"
    (is (true?
         (target/passphrase-distinct-words? "aa bb cc dd ee")))

    (is (false?
         (target/passphrase-distinct-words? "aa bb cc dd aa")))

    (is (true?
         (target/passphrase-distinct-words? "aa bb cc dd aaa")))))



(deftest count-of-valid-passphrases
  (testing "When passphrases are empty"
    (testing "Should be false"
      (is (= 0
             (target/count-of-valid-passphrases nil)))

      (is (= 0
             (target/count-of-valid-passphrases [])))))

  (testing "When passphrases have content"
    (is (= 1
           (target/count-of-valid-passphrases ["aa bb cc dd ee"])))

    (is (= 2
           (target/count-of-valid-passphrases ["aa bb cc dd ee"
                                               "aa bb"])))

    (is (= 2
           (target/count-of-valid-passphrases ["aa bb cc dd ee"
                                               "aa bb"
                                               "aa aa"])))

    (is (= 3
           (target/count-of-valid-passphrases ["aa bb cc dd ee"
                                               "aa bb"
                                               "aa aaa"])))
    ))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Part 2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftest passphrase-without-anagram?
  (testing "When passphrase is empty"
    (testing "Should be false"
      (is (false?
           (target/passphrase-without-anagram? nil)))

      (is (false?
           (target/passphrase-without-anagram? "")))))


  (testing "When passphrase has content"
    (is (true?
         (target/passphrase-without-anagram? "abcde fghij")))

    (is (false?
         (target/passphrase-without-anagram? "abcde xyz ecdab")))

    (is (true?
         (target/passphrase-without-anagram? "a ab abc abd abf abj")))

    (is (true?
         (target/passphrase-without-anagram? "iiii oiii ooii oooi oooo")))

    (is (false?
         (target/passphrase-without-anagram? "oiii ioii iioi iiio")))
    ))



(deftest count-of-valid-passphrases2
  (testing "When passphrases are empty"
    (testing "Should be false"
      (is (= 0
             (target/count-of-valid-passphrases2 nil)))

      (is (= 0
             (target/count-of-valid-passphrases2 [])))))

  (testing "When passphrases have content"
    (is (= 1
         (target/count-of-valid-passphrases2 ["abcde fghij"])))

    (is (= 0
         (target/count-of-valid-passphrases2 ["abcde xyz ecdab"])))

    (is (= 2
           (target/count-of-valid-passphrases2 ["abcde fghij"
                                                "abcde xyz ecdab"
                                                "a ab abc abd abf abj"])))

    (is (= 3
           (target/count-of-valid-passphrases2 ["abcde fghij"
                                                "abcde xyz ecdab"
                                                "a ab abc abd abf abj"
                                                "iiii oiii ooii oooi oooo"])))

    (is (= 3
           (target/count-of-valid-passphrases2 ["abcde fghij"
                                                "abcde xyz ecdab"
                                                "a ab abc abd abf abj"
                                                "iiii oiii ooii oooi oooo"
                                                "oiii ioii iioi iiio"])))
    ))
